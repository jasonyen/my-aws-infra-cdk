#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { CdkEcsInfraStack, CdkEcsInfraStackProps } from '../lib/cdk-ecs-infra-stack';

const app = new cdk.App();
// new CdkEcsInfraStack(app, 'CdkEcsInfraStack', {
//   /* If you don't specify 'env', this stack will be environment-agnostic.
//    * Account/Region-dependent features and context lookups will not work,
//    * but a single synthesized template can be deployed anywhere. */

//   /* Uncomment the next line to specialize this stack for the AWS Account
//    * and Region that are implied by the current CLI configuration. */
//   // env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },

//   /* Uncomment the next line if you know exactly what Account and Region you
//    * want to deploy the stack to. */
//   env: { account: '871336434060', region: 'ap-southeast-1' },

//   /* For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html */
// });

const apSoutheast1StackProps: CdkEcsInfraStackProps = {
  env: {
    account: '871336434060', // process.env.CDK_DEFAULT_ACCOUNT,
    region: 'ap-southeast-1'
  },
  region: 'ap-southeast-1'
};
new CdkEcsInfraStack(app, 'CdkEcsInfraStackApSoutheast1', apSoutheast1StackProps);

const usWest2StackProps: CdkEcsInfraStackProps = {
  env: {
    account: '871336434060', // process.env.CDK_DEFAULT_ACCOUNT,
    region: 'us-west-2'
  },
  region: 'us-west-2'
};
new CdkEcsInfraStack(app, 'CdkEcsInfraStackUsWest2', usWest2StackProps);

app.synth();