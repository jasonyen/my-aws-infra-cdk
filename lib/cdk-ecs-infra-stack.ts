import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as ecs from "aws-cdk-lib/aws-ecs";
import * as ecs_patterns from "aws-cdk-lib/aws-ecs-patterns";
import { ApplicationProtocol, ApplicationProtocolVersion } from 'aws-cdk-lib/aws-elasticloadbalancingv2';

export interface CdkEcsInfraStackProps extends cdk.StackProps {
  region: string;
}

export class CdkEcsInfraStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // Look up the default VPC
    // const vpc = ec2.Vpc.fromLookup(this, "CdkDemoStack/mainVPC", {
    //   isDefault: true
    // });
    const vpc = new ec2.Vpc(this, 'JasonWebsiteVpc', {
      cidr: '10.0.0.0/16', // CIDR block for the VPC
      maxAzs: 2, // Maximum number of Availability Zones to use
      subnetConfiguration: [
        {
          subnetType: ec2.SubnetType.PUBLIC,
          name: 'Public',
          cidrMask: 24
        },
        {
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
          name: 'Isolated',
          cidrMask: 24
        }
      ],
      natGateways: 1 // Number of NAT gateways to create
    });

    // Create a Security Group
    const securityGroup = new ec2.SecurityGroup(this, 'MyTestSecurityGroup', {
      vpc,
      description: 'My custom test security group',
      allowAllOutbound: false, // Set to false to restrict outbound traffic
    });

    // Create a Security Group for ALB
    const loadBalancerSecurityGroup = new ec2.SecurityGroup(this, 'MyLoadBalancerSecurityGroup', {
      vpc,
      description: 'My custom load balancer security group',
      allowAllOutbound: true,
    });

    // Add an inbound rule to allow incoming traffic on a specific port
    securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(80), 'Allow HTTP traffic from any IPv4 address');
    securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(8080), 'Allow HTTP traffic from any IPv4 address');
    securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(443), 'Allow HTTPs traffic from any IPv4 address');
    securityGroup.addIngressRule(loadBalancerSecurityGroup, ec2.Port.tcp(80), 'Allow HTTP traffic from the load balancer');

    // Add an outbound rule to allow outgoing traffic to a specific IP range and port
    securityGroup.addEgressRule(ec2.Peer.ipv4('0.0.0.0/0'), ec2.Port.tcp(4000), 'Allow HTTP traffic to the 0.0.0.0/0 IP range');
    securityGroup.addEgressRule(ec2.Peer.ipv4('0.0.0.0/0'), ec2.Port.tcp(3000), 'Allow HTTP traffic to the 0.0.0.0/0 IP range');
    securityGroup.addEgressRule(ec2.Peer.ipv4('0.0.0.0/0'), ec2.Port.tcp(443), 'Allow HTTPS traffic to the 0.0.0.0/0 IP range');

    // Create the task definition and IAM role
    const taskIamRole = new cdk.aws_iam.Role(this, "AppRole", {
      roleName: `AppRole-${this.region}`,
      assumedBy: new cdk.aws_iam.ServicePrincipal('ecs-tasks.amazonaws.com'),
    });

    const taskDefinition = new ecs.FargateTaskDefinition(this, 'Task', {
      taskRole: taskIamRole,
      memoryLimitMiB: 8192,
      cpu: 4096
    });

    taskDefinition.addContainer('MyContainer', {
      image: ecs.ContainerImage.fromAsset('../SampleApp', {
        platform: cdk.aws_ecr_assets.Platform.LINUX_AMD64
      }),
      portMappings: [{ containerPort: 4000 }],
      memoryLimitMiB: 4096,
      cpu: 2048
    });

    new ecs_patterns.ApplicationLoadBalancedFargateService(this, "MyECSApp", {
      vpc: vpc,
      taskDefinition: taskDefinition,
      desiredCount: 1,
      serviceName: 'MyCDKDemoWebApp1',
      assignPublicIp: true,
      publicLoadBalancer: true,
      securityGroups: [securityGroup], // Add your custom security group here
      listenerPort: 80,
      protocol: ApplicationProtocol.HTTP,
    })
  }
}
